﻿using System;
using System.Collections.Generic;
using System.Text;
using Xamarin.Forms;

namespace CS481HW4
{
    public class AnimeCharacters
    {
        public string Name { get; set; }
        public string AnimeName { get; set; }
        public ImageSource AnimeImage { get; set; }

    }
}
