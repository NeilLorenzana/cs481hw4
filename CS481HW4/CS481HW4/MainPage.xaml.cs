﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Xamarin.Forms;
using System.Collections.ObjectModel;

namespace CS481HW4
{
    public partial class MainPage : ContentPage
    {
        public ObservableCollection<AnimeCharacters> CharacterList { get; set; }


        public MainPage()
        {
            InitializeComponent();
            PopulateList();
        }

        private void PopulateList()
        {
            CharacterList = new ObservableCollection<AnimeCharacters>()
            {
                new AnimeCharacters()
                {
                Name = "Tohru",
                AnimeName = "Miss Kobayashi's Dragon Maid",
                AnimeImage = "tohru.jpg"

                },

                new AnimeCharacters()
                {
                Name = "Rio Futaba",
                AnimeName = "Rascal Does Not Dream of Bunny Girl Senpai",
                AnimeImage = "futabaa.png"

                },

                new AnimeCharacters()
                {
                Name = "Kaho Hinata",
                AnimeName = "Blend S",
                AnimeImage = "kaho.jpg"

                },

                new AnimeCharacters()
                {
                Name = "Taiga Aisaka",
                AnimeName = "Toradora",
                AnimeImage = "taiga.jpg"

                },

                new AnimeCharacters()
                {
                Name = "Yoko Littner",
                AnimeName = "Gurren Lagann",
                AnimeImage = "yokoo.jpg"

                }

            };

            CharacterListView.ItemsSource = CharacterList;
        }

        void Handle_Refreshing(object sender, System.EventArgs e)
        {
            PopulateList();
            CharacterListView.IsRefreshing = false;
        }

        void Handle_DeleteCharacter(object sender, EventArgs e)
        {
            var menuItem = (MenuItem)sender;
            var characterDelete = (AnimeCharacters)menuItem.CommandParameter;
            CharacterList.Remove(characterDelete);
        }


    }
}
